#!/bin/bash -ex

# CONFIG
prefix="Alfred"
suffix=""
munki_package_name="Alfred"
display_name="Alfred"
category="Productivity"
description="Alfred is an award-winning app for Mac OS X which boosts your efficiency with hotkeys and keywords. Search your Mac and the web effortlessly, and control your Mac using customised actions with the Powerpack."
url=`./finder.rb`

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36' "${url}"

## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
#/usr/sbin/pkgutil --expand "${mountpoint}/*.pkg" pkg
mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R "${app_in_dmg}" build-root/Applications

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" "${app_in_dmg}"/Contents/Info.plist`
#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --version ${version} app.pkg

# Build pkginfo
/usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg -f build-root/Applications/Alfred*.app > app.plist
plist=`pwd`/app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.12"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" version "${version}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
